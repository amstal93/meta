terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/19088702/terraform/state/meta"
    lock_address   = "https://gitlab.com/api/v4/projects/19088702/terraform/state/meta/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/19088702/terraform/state/meta/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"

  }
}