#!/bin/bash

for repo in ~/Repos/the-junkyard/*; do
  echo $repo
  cp -r issue_templates/ $repo/.gitlab && cd $repo && git stage .gitlab && git commit -m "updated .gitlab"; git push; cd ~/Repos/terraform/the-junkyard
done
