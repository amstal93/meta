# Common variables
variable "secrets" {
  type      = map(string)
  sensitive = true
  default = {
    "key" = "secret_Value"
  }
}

variable "emails" {
  description = "emails that are used to in services for notifications and accounts."
  default = {
    alerts = "alerts@acpatt.com"
  }
}

# gitlab.tf variables
variable "group" {
  description = "a gitlab group"
  type = object({
    name             = string
    path             = string
    description      = string
    visibility_level = string
  })
  default = {
    "name"             = "The junkyard"
    "path"             = "the-junkyard"
    "description"      = "A group to contain each of the servers in the junkyard."
    "visibility_level" = "public"
  }
}

variable "projects" {
  description = "a gitlab project"
  type = list(object({
    name               = string
    path               = string
    description        = string
    visibility_level   = string
    tags               = list(string)
    contained          = bool
    set_vault_password = bool
  }))
  default = [{
    name               = "Test Project"
    path               = "test-project"
    description        = "test project description"
    visibility_level   = "public"
    tags               = ["default"]
    contained          = false
    set_vault_password = false

  }]
}

locals {
  label-types = yamldecode(file("vars.yaml"))["label-types"]
  projects    = yamldecode(file("vars.yaml"))["projects"]
}

variable "vault_passes" {
  type = map(string)
  default = {
    "key" = "secret_Value"
  }
}

# cloudflare.tf variables

variable "home_ip" {
  description = "the IP of my house"
  default     = "212.159.47.88"
}

# uptimerobot.tf variables

# digitalocean variables

variable "id_rsa_pub" {
  description = "rsa key for digitalocean"
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDuJs/Jy1uDaxH+0tYaSd1KqWkBzZficxGaud1+hcJCVwCxRQtA0E3ideA6C2sjSLpb5ASupURbeW2je4zIGU+0TgcVFfapw24NwAkhuw0/zw4VC6Q+KQfhNC+BTf/rCy2drZxzkgG72dL3ZT9oK6Ll+rPfUKw/Jt5DZqLChrMyfvRRzFZtTjJvl+6R/r5J0ONjvecfjSNpnRubddG1YPK5dun0Pn0GhK/xxkcwD5Ktxd0mmgWcc3KJEAAqs3l/G56MmOkOQuRnY1cWT1Km0tEF/+SYXDJyufEoZ95ZLhpibX3zMJcq9fhzkawlineoe/AZbue2e0ZW1hMkcuWBPBVh acpatt@kestrel"
}
