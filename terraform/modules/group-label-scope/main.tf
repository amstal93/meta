
terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
  required_version = ">= 0.13"
}



resource "gitlab_group_label" "scoped" {
  count       = length(var.label-type.values)
  group       = var.group-id
  name        = join("", [var.label-type.key, "::", var.label-type.values[count.index]])
  description = var.label-type.description
  color       = var.label-type.colour
}
