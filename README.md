# The Junkyard

This project contains the infrastructure code which provides the groundworks for the rest of the stuff here. It provides code for the DNS for acpatt.com, the Uptimerobot account and defaults and, of course, all the Gitlab projects and some associated gitlab gubbins like branch protection and such.